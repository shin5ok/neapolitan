#!/usr/bin/env perl
my $new  = '';
my @keys = 'neapolitan' =~ /(\w)/g;
my $k = shift @keys;
for my $t (
'gtgtsgipgttptinggipsppaigsesgpetgstpatetisiesagaeaigttetepitiatsegssieeeeatepaaiagtpieataatppiitgiapsteitatiiatpetetetttgpetpaasipttssstpeeeggtiagtttegtiipestsasgpsepaasapttgattgiatppegitiatpasgatgepttggapesaeetaeissttggieietgspagesiipestipggstttpateptitiaetottissgggtttaipappgstsptttgtpispattgegstltiappseisapgistaiagteeiptptpisaieisagstapeteietgteiisgtiptstgtstasspeatspptitttatteastsgtptgtasggpniaaeteaisett' =~ /(\w)/g ) {
  if ($k and $t eq $k) {
    $new .= "[$k]";
    $k = shift @keys;
    next;
  }
  $new .= $t;
}

print $new, "\n";
